package ilacqua.intervaltree_nullless.tree;

import java.util.Map.Entry;

import ilacqua.intervaltree_nullless.data.Interval;
import ilacqua.intervaltree_nullless.data.SmartInterval;
import ilacqua.intervaltree_nullless.data.SmartInterval.Point;

import java.util.NavigableMap;
import java.util.TreeMap;

public class SmartIntervalTree implements IIntervalTree {

	private TreeMap<SmartInterval, Void> intervals;

	public SmartIntervalTree() {
		intervals = new TreeMap<SmartInterval, Void>(SmartInterval.getComparator(true, false, false));
	}

	public SmartIntervalTree(boolean sortByTo) {
		intervals = new TreeMap<SmartInterval, Void>(SmartInterval.getComparator(sortByTo, sortByTo, sortByTo));
	}

	@Override
	public void clear() {
		intervals.clear();
	}

	@Override
	public int getSize() {
		return intervals.size();
	}

	@Override
	public String toString() {
		return intervals.keySet().toString();
	}

	public boolean isInInterval(Integer item) {
		return findInterval(item).isValid();
	}

	@Override
	public SmartInterval findInterval(Integer item) {
		return findInterval(new Point(item));
	}

	private SmartInterval findInterval(Point point) {
		Entry<SmartInterval, Void> potentialInterval = intervals.lowerEntry(point);
		if (potentialInterval != null && potentialInterval.getKey().surrounds(point)) {
			return potentialInterval.getKey();
		} else {
			return SmartInterval.getEmptyInterval();
		}
	}

	@Override
	public void insert(Interval incomingInterval) {
		SmartInterval targetInterval = SmartInterval.from(incomingInterval);

		if (!targetInterval.isValid()) {
			return;
		}
		// also check for if current one is already present

		SmartInterval clearInterval = getClearInterval(targetInterval);
		SmartInterval addInterval = clearInterval.isValid() ? clearInterval : targetInterval;

		if (addInterval.isValid()) {
			if (clearInterval.isValid()) {
				getSubMap(clearInterval).clear();
			}
			intervals.put(addInterval, null);
		}
	}

	private SmartInterval getClearInterval(SmartInterval interval) {
		Point clearFrom = getIntervalStart(interval.getFromPoint());
		Point clearTo = getIntervalEnd(interval.getStartPoint());
		return SmartInterval.join(clearFrom, clearTo);
	}

	private Point getIntervalEnd(Point point) {
		SmartInterval newInterval = findInterval(point);
		return newInterval.isValid() ? newInterval.getStartPoint() : point;
	}

	private Point getIntervalStart(Point point) {
		SmartInterval newInterval = findInterval(point);
		return newInterval.isValid() ? newInterval.getFromPoint() : point;
	}

	private NavigableMap<SmartInterval, Void> getSubMap(SmartInterval targetInterval) {
		return intervals.subMap(targetInterval.getFromPoint(), true, targetInterval.getStartPoint(), true);
	}
}