package ilacqua.intervaltree_nullless.tree;

import ilacqua.intervaltree_nullless.data.Interval;

public interface IIntervalTree {

	void insert(Interval interval);

	boolean isInInterval(Integer item);

	Interval findInterval(Integer item);

	void clear();

	int getSize();
}
