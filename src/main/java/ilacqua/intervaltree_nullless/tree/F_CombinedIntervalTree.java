package ilacqua.intervaltree_nullless.tree;

import java.util.Map.Entry;

import ilacqua.intervaltree_nullless.data.F_Interval;
import ilacqua.intervaltree_nullless.data.F_Interval.SingleInterval;

import java.util.NavigableMap;
import java.util.TreeMap;

public class F_CombinedIntervalTree {

	// is there a red black tree without being a map
	private TreeMap<F_Interval, Void> intervals = new TreeMap<F_Interval, Void>();
	private boolean isMergeOverlap;

	public F_CombinedIntervalTree() {
		this(true);
	}

	public F_CombinedIntervalTree(boolean mergeOverlap) {
		this.isMergeOverlap = mergeOverlap;
	}

	public void add(Integer from, Integer to) {
		add(new F_Interval(from, to));
		System.out.println(toString());

	}

	public void add(F_Interval targetInterval) {
		if (!targetInterval.isValid()) {
			return;
		}
		// also check for if current one is already present

		F_Interval clearInterval = getClearInterval(targetInterval);
		F_Interval addInterval = getAddInterval(targetInterval, clearInterval);

		// add invterval should never be invalid
		if (addInterval.isValid()) {
			if (clearInterval.isValid()) {
				clearExistingIntervals(clearInterval);
			}
			addNewInterval(addInterval);
		}
	}

	private void addNewInterval(F_Interval addInterval) {
		intervals.put(addInterval, null);
	}

	private void clearExistingIntervals(F_Interval clearInterval) {
		NavigableMap<F_Interval, Void> subMap = intervals.subMap(clearInterval, true, clearInterval.getToInterval(),
				true);
		subMap.clear();
	}

	private F_Interval getClearInterval(F_Interval targetInterval) {
		SingleInterval clearFrom = getClearFrom(targetInterval);
		SingleInterval clearTo = getClearTo(targetInterval);
		return F_Interval.join(clearFrom, clearTo);
	}

	private F_Interval getAddInterval(F_Interval targetInterval, F_Interval clearInterval) {
		return isMergeOverlap && clearInterval.isValid() ? clearInterval : targetInterval;
	}

	private SingleInterval getClearFrom(F_Interval interval) {
		if (isMergeOverlap) {
			return getLowerFrom(interval.getFromInterval());
		} else {
			return interval.getFromInterval();
		}
	}

	private SingleInterval getClearTo(F_Interval targetInterval) {
		if (isMergeOverlap) {
			return getLowerTo(targetInterval.getToInterval());
		} else {
			NavigableMap<F_Interval, Void> subMap = getSubMap(targetInterval);
			for (F_Interval intervalWalker : subMap.descendingMap().keySet()) {
				if (intervalWalker.surrounds(targetInterval)) {
					return intervalWalker.getToInterval();
				}
			}
			return targetInterval.getToInterval();
		}
	}

	private NavigableMap<F_Interval, Void> getSubMap(F_Interval targetInterval) {
		return intervals.subMap(targetInterval.getFromInterval(), true, targetInterval.getToInterval(), true);
	}

	private SingleInterval getLowerTo(SingleInterval targetInterval) {
		F_Interval newInterval = getSurroundingInterval(targetInterval);
		return newInterval.isValid() ? newInterval.getToInterval() : targetInterval.getToInterval();
	}

	private SingleInterval getLowerFrom(SingleInterval targetInterval) {
		F_Interval newInterval = getSurroundingInterval(targetInterval);
		return newInterval.isValid() ? newInterval.getFromInterval() : targetInterval.getFromInterval();
	}

	// not really getting surrounding interval, but immediately preceding that
	// surrounds
	private F_Interval getSurroundingInterval(SingleInterval targetInterval) {
		F_Interval potentialInterval = getLowerEntryInterval(targetInterval);
		if (potentialInterval.surrounds(targetInterval)) {
			return potentialInterval;
		}
		return F_Interval.getEmptyInterval();
	}

	@Override
	public String toString() {
		return intervals.keySet().toString();
	}

	public boolean isInInterval(Integer i) {
		// only valid with merge
		SingleInterval key = new SingleInterval(i);
		return getSurroundingInterval(key).isValid();
	}

	// not done
	public boolean isOverlapPresent(Integer i) {
		SingleInterval targetInterval = new SingleInterval(i);
		return getLowerEntryInterval(targetInterval).surrounds(targetInterval);
		// only valid with merge

		// // essentially:
		// Entry<Interval, Void> lowerEntry = intervals.lowerEntry(key);
		// if (lowerEntry != null) {
		// Interval interval = lowerEntry.getKey();
		// // if key to is above lowerEntry from, figure out right way
		//
		// // also try to get
		// // Entry<Interval, Void> lowerEntry = intervals.lowerEntry(key);
		// // if (lowerEntry != null) {
		//
		// if (interval.surrounds(targetInterval)) {
		// return interval;
		// }
		// }
		// return Interval.getEmptyInterval();
		// SingleInterval key = new SingleInterval(i);
		// return getSurroundingInterval(key).isValid();
	}

	// mcuh better

	public F_Interval getLowerEntryInterval(F_Interval interval) {
		Entry<F_Interval, Void> lowerEntry = intervals.lowerEntry(interval);
		return lowerEntry != null ? lowerEntry.getKey() : F_Interval.getEmptyInterval();
	}

}