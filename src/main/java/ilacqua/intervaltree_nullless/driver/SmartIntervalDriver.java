package ilacqua.intervaltree_nullless.driver;

import java.util.LinkedHashMap;
import java.util.Map;

import ilacqua.intervaltree_nullless.tree.F_CombinedIntervalTree;

public class SmartIntervalDriver {

	Map<Integer, Integer> inputMap = new LinkedHashMap<Integer, Integer>();
	F_CombinedIntervalTree intervalTree;

	public static void main(String... args) {
		new SmartIntervalDriver().run();
	}

	private void run() {
		createIntervalTree();
		setInput();
		runInput();
		test();
	}

	private void createIntervalTree() {
		// intervalTree = new IntervalTree();
		intervalTree = new F_CombinedIntervalTree(true);
	}

	private void setInput() {
		// inputMap.put(37, 69);
		// inputMap.put(60, 75);

		inputMap.put(37, 69);
		inputMap.put(190, 235);
		inputMap.put(270, 280);
		inputMap.put(250, 260);
		inputMap.put(200, 290);
		inputMap.put(1, 2);

		System.out.println("Initial Data " + inputMap);
	}

	private void runInput() {
		inputMap.forEach(this::insertIntervals);
		System.out.println("End Data " + intervalTree);
	}

	private void insertIntervals(Integer from, Integer to) {
		intervalTree.add(from, to);
	}

	private void test() {
		// System.out.println("yes " + intervalTree.isInInterval(50));
		// System.out.println("no " + intervalTree.isInInterval(300));
		// System.out.println("no " + intervalTree.isInInterval(-50));
		// System.out.println("no " + intervalTree.isInInterval(80));
		System.out.println("yes " + intervalTree.isInInterval(190));
		System.out.println("yes " + intervalTree.isInInterval(200));
		System.out.println("yes " + intervalTree.isInInterval(290));
	}
}
