package ilacqua.intervaltree_nullless.data;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

@SuppressWarnings("serial")
public class Interval extends SimpleEntry<Integer, Integer> {

	protected Interval(Integer left, Integer right) {
		super(left, right);
	}

	public static Interval of(Integer left, Integer right) {
		return new Interval(left, right);
	}

	public static Interval from(Entry<Integer, Integer> entry) {
		return of(entry.getKey(), entry.getValue());
	}

	public Interval clone(int offset) {
		return new Interval(getStart() + offset, getEnd() + offset);
	}

	public Integer getStart() {
		return getKey();
	}

	public Integer getEnd() {
		return getValue();
	}

	public String toString() {
		return "(" + getStart() + "=" + getEnd() + ")";
	}

}
