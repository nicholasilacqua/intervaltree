package ilacqua.intervaltree_nullless.data;

import java.util.Comparator;
import java.util.Map.Entry;

@SuppressWarnings("serial")
public class SmartInterval extends Interval {

	public static class Point extends SmartInterval {
		public Point(Integer fromAndTo) {
			super(fromAndTo, fromAndTo);
		}
	}

	protected SmartInterval() {
		this(null, null);
	}

	protected SmartInterval(Integer from, Integer to) {
		super(from, to);
	}

	// find way to not need to override

	public static SmartInterval getEmptyInterval() {
		return new SmartInterval(null, null);
	}

	public static SmartInterval of(Integer start, Integer end) {
		if (isValid(start, end)) {
			return new SmartInterval(start, end);
		} else {
			return getEmptyInterval();
		}
	}

	public static SmartInterval from(Entry<Integer, Integer> entry) {
		return of(entry.getKey(), entry.getValue());
	}

	public SmartInterval clone(int offset) {
		return SmartInterval.of(getStart() + offset, getEnd() + offset);
	}

	// Validation

	public boolean isValid() {
		return isValid(getStart(), getEnd());
	}

	public boolean isValidAction(SmartInterval argument) {
		return isValid() && argument.isValid();
	}

	private static boolean isValid(Integer from, Integer to) {
		return from != null && to != null && from <= to;
	}

	// Mapping with Single Interval

	public static SmartInterval join(Point fromInterval, Point toInterval) {
		return SmartInterval.of(fromInterval.getStart(), toInterval.getEnd());
	}

	public Point getStartPoint() {
		return new Point(getEnd());
	}

	public Point getFromPoint() {
		return new Point(getStart());
	}

	// Comparison

	// isValidAction I added on the new version, doesn't seem appropriate

	// TODO : isAbove andisBelow acting properly, should be switched?
	public boolean isAbove(Point target) {
		return isValidAction(target) && target.getEnd() < getEnd();
	}

	public boolean isBelow(Point target) {
		return isValidAction(target) && target.getStart() > getStart();
	}

	public boolean surrounds(SmartInterval target) {
		return isValidAction(target) && target.getStart() >= getStart() && target.getEnd() <= getEnd();
	}

	// Sorting

	// @Override
	// public int compareTo(F_AbstractSecond o) {
	// return Integer.compare(getEnd(), o.getEnd());
	// }
	//
	// public int compareFrom(F_AbstractSecond o) {
	// return Integer.compare(getStart(), o.getStart());
	// }

	private static int compareUsingTo(SmartInterval first, SmartInterval second) {
		return Integer.compare(first.getEnd(), second.getEnd());
	}

	private static int compareUsingFrom(SmartInterval first, SmartInterval second) {
		return Integer.compare(first.getStart(), second.getStart());
	}

	private static int compareUsingToThenFrom(SmartInterval first, SmartInterval second) {
		int compare = compareUsingTo(first, second);
		if (compare == 0) {
			compareUsingFrom(first, second);
		}
		return compare;
	}

	private static int compareUsingFromThenTo(SmartInterval first, SmartInterval second) {
		int compare = Integer.compare(first.getStart(), second.getStart());
		return compare;
	}

	public static Comparator<SmartInterval> getComparator(boolean usingTo, boolean reverse, boolean useBoth) {
		Comparator<SmartInterval> comparator = usingTo ? SmartInterval::compareUsingTo
				: SmartInterval::compareUsingFrom;
		return reverse ? comparator.reversed() : comparator;
	}
}
