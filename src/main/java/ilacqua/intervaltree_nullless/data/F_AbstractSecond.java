package ilacqua.intervaltree_nullless.data;

public class F_AbstractSecond implements Comparable<F_AbstractSecond> {

	public static class SingleInterval extends F_AbstractSecond {
		public SingleInterval(Integer fromAndTo) {
			super(fromAndTo, fromAndTo);
		}
	}

	protected Integer from = null;
	protected Integer to = null;

	private F_AbstractSecond() {
	}

	public F_AbstractSecond(Integer from, Integer to) {
		if (isValid(from, to)) {
			this.from = from;
			this.to = to;
		} else {
			// new Exception("Bad Validation").printStackTrace();
		}
	}

	// Validation

	public boolean isValid() {
		return isValid(from, to);
	}

	private static boolean isValid(Integer from, Integer to) {
		return from != null && to != null && from <= to;
	}

	// Mapping with Single Interval

	public static F_AbstractSecond join(SingleInterval fromInterval, SingleInterval toInterval) {
		return new F_AbstractSecond(fromInterval.from, toInterval.to);
	}

	public SingleInterval getToInterval() {
		return new SingleInterval(to);
	}

	public SingleInterval getFromInterval() {
		return new SingleInterval(from);
	}

	public static F_AbstractSecond getEmptyInterval() {
		return new F_AbstractSecond();
	}

	// Comparison

	// @Override
	// public int compareTo(AbstractSecond o) {
	// return Integer.compare(from, o.from);
	// }

	// TODO : isAbove andisBelow acting properly, should be switched?
	public boolean isAbove(SingleInterval target) {
		return target.to < to;
	}

	public boolean isBelow(SingleInterval target) {
		return target.from > from;
	}

	public boolean surrounds(F_AbstractSecond target) {
		return target.from >= from && target.to <= to;
	}

	// Utilities
	@Override
	public String toString() {
		return "(" + from + ", " + to + ")";
	}

	@Override
	public int compareTo(F_AbstractSecond o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
