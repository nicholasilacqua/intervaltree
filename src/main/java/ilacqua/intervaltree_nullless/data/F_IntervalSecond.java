package ilacqua.intervaltree_nullless.data;

public class F_IntervalSecond implements Comparable<F_IntervalSecond> {

	public static class SingleInterval extends F_IntervalSecond {
		public SingleInterval(Integer fromAndTo) {
			super(fromAndTo, fromAndTo);
		}
	}

	protected Integer from = null;
	protected Integer to = null;

	private F_IntervalSecond() {
	}

	public F_IntervalSecond(Integer from, Integer to) {
		if (isValid(from, to)) {
			this.from = from;
			this.to = to;
		} else {
			// new Exception("Bad Validation").printStackTrace();
		}
	}

	// Validation

	public boolean isValid() {
		return isValid(from, to);
	}

	private static boolean isValid(Integer from, Integer to) {
		return from != null && to != null && from <= to;
	}

	// Mapping with Single Interval

	public static F_IntervalSecond join(SingleInterval fromInterval, SingleInterval toInterval) {
		return new F_IntervalSecond(fromInterval.from, toInterval.to);
	}

	public SingleInterval getToInterval() {
		return new SingleInterval(to);
	}

	public SingleInterval getFromInterval() {
		return new SingleInterval(from);
	}

	public static F_IntervalSecond getEmptyInterval() {
		return new F_IntervalSecond();
	}

	// Comparison

	@Override
	public int compareTo(F_IntervalSecond o) {
		return Integer.compare(from, o.from);
	}

	// TODO : isAbove andisBelow acting properly, should be switched?
	public boolean isAbove(SingleInterval target) {
		return target.to < to;
	}

	public boolean isBelow(SingleInterval target) {
		return target.from > from;
	}

	public boolean surrounds(F_IntervalSecond target) {
		return target.from >= from && target.to <= to;
	}

	// Utilities
	@Override
	public String toString() {
		return "(" + from + ", " + to + ")";
	}
}
