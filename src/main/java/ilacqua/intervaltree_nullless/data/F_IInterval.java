package ilacqua.intervaltree_nullless.data;

public interface F_IInterval {
	Integer getStart();

	Integer getEnd();

	default String toPrettyFormat() {
		return "(" + getStart() + "=" + getEnd() + ")";
	}
}
