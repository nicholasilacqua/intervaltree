package ilacqua.intervaltree_nullless.data;

import java.util.Comparator;

public class F_Interval implements Comparable<F_Interval> {

	public static class SingleInterval extends F_Interval {
		public SingleInterval(Integer fromAndTo) {
			super(fromAndTo, fromAndTo);
		}
	}

	protected Integer from = null;
	protected Integer to = null;

	private F_Interval() {
	}

	public F_Interval(Integer from, Integer to) {
		if (isValid(from, to)) {
			this.from = from;
			this.to = to;
		} else {
			// new Exception("Bad Validation").printStackTrace();
		}
	}

	// Validation

	public boolean isValid() {
		return isValid(from, to);
	}

	private static boolean isValid(Integer from, Integer to) {
		return from != null && to != null && from <= to;
	}

	// Mapping with Single Interval

	public static F_Interval join(SingleInterval fromInterval, SingleInterval toInterval) {
		return new F_Interval(fromInterval.from, toInterval.to);
	}

	public SingleInterval getToInterval() {
		return new SingleInterval(to);
	}

	public SingleInterval getFromInterval() {
		return new SingleInterval(from);
	}

	public static F_Interval getEmptyInterval() {
		return new F_Interval();
	}

	// Comparison

	// TODO : isAbove andisBelow acting properly, should be switched?
	public boolean isAbove(SingleInterval target) {
		return isValid() && target.to < to;
	}

	public boolean isBelow(SingleInterval target) {
		return isValid() && target.from > from;
	}

	public boolean surrounds(F_Interval target) {
		return isValid() && target.from >= from && target.to <= to;
	}

	// Utilities
	@Override
	public String toString() {
		return "(" + from + ", " + to + ")";
	}

	// Sorting

	// public int compareTo(Object o) {
	// if (o instanceof F_AbstractSecond) {
	// return compareTo((F_AbstractSecond) o);
	// }
	// return -1;
	// }

	@Override
	public int compareTo(F_Interval o) {
		return Integer.compare(to, o.to);
	}

	public int compareFrom(F_AbstractSecond o) {
		return Integer.compare(from, o.from);
	}

	private static int compareUsingTo(F_Interval first, F_Interval second) {
		return Integer.compare(first.to, second.to);
	}

	private static int compareUsingFrom(F_Interval first, F_Interval second) {
		return Integer.compare(first.from, second.from);
	}

	private static int compareUsingToThenFrom(F_Interval first, F_Interval second) {
		int compare = compareUsingTo(first, second);
		if (compare == 0) {
			compareUsingFrom(first, second);
		}
		return compare;
	}

	private static int compareUsingFromThenTo(F_Interval first, F_Interval second) {
		int compare = Integer.compare(first.from, second.from);
		return compare;
	}

	public static Comparator<F_Interval> getComparator(boolean usingTo, boolean reverse, boolean useBoth) {
		Comparator<F_Interval> comparator = usingTo ? F_Interval::compareUsingTo : F_Interval::compareUsingFrom;
		return reverse ? comparator.reversed() : comparator;
	}
}
