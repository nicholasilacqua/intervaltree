package ilacqua.intervaltreeCONDENSED.driver;

import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import ilacqua.intervaltreeCONDENSED.data.Interval;
import ilacqua.intervaltreeCONDENSED.tree.FullIntervalTree;
import ilacqua.intervaltreeCONDENSED.util.IntervalIO;
import ilacqua.intervaltreeCONDENSED.util.TestDataUtil;
import ilacqua.intervaltreeCONDENSED.util.TestValidationUtil;

public class TestDriver {

	private static final String DEFAULT_FILENAME = "testData.csv";;

	public static void main(String... args) {
		new TestDriver().run(args);
	}

	private void run(String[] args) {
		String action = "run";
		String outputfilename = null;
		String inputfilename = null;

		int paramWalker = Math.min(args.length, 3);
		switch (paramWalker) {
		case 3:
			inputfilename = args[2];
		case 2:
			outputfilename = args[1];
		case 1:
			action = args[0];
		default:
			// no params
		}

		if (StringUtils.isBlank(action) || action.equals("run")) {
			runIntervalTree(outputfilename, inputfilename);
		} else if (action.equals("random")) {
			createRandomIntervalFile(outputfilename);
		} else {
			System.out.println("Usage : java ??? action outputfile inputfile");
			System.out.println("action = run or random(default to run)");
			System.out.println("outputfile = file to receive processed intervals");
			System.out.println("inputfile = file to provide intervals to be processeed(not use with action=random)");
			System.out.println("All options are optional, but when present order is strictly adhered to determine which option it is");
		}
	}

	private void createRandomIntervalFile(String outputfilename) {
		List<Interval> intervals = TestDataUtil.getRandomIntervals();
		String fileToOutputRandom = StringUtils.isNotBlank(outputfilename) ? outputfilename : DEFAULT_FILENAME;
		IntervalIO.writeIntervalsToFile(fileToOutputRandom, intervals);
	}

	private void runIntervalTree(String outputfilename, String inputfilename) {
		runHeader();
		FullIntervalTree intervalTree = new FullIntervalTree();

		List<Interval> inputIntervals = getInputIntervals(inputfilename);
		List<Interval> outputIntervals = runTree(intervalTree, inputIntervals);

		runValidation(intervalTree, inputIntervals, outputIntervals);
		runReport(inputIntervals, outputIntervals);
		createOutput(outputfilename, outputIntervals);
	}

	private void runHeader() {
		System.out.println("************************************************");
		System.out.println();
		System.out.println("------- INTERVAL TREE -------");
		System.out.println();
	}

	private List<Interval> getInputIntervals(String inputfilename) {
		List<Interval> inputIntervals;
		if (StringUtils.isNotBlank(inputfilename)) {
			inputIntervals = IntervalIO.readIntervalsFromFile(inputfilename);
		} else {
			inputIntervals = TestDataUtil.getRandomIntervals();
		}
		return inputIntervals;
	}

	private List<Interval> runTree(FullIntervalTree intervalTree, List<Interval> inputIntervals) {
		inputIntervals.forEach(intervalTree::insert);
		List<Interval> outputIntervals = intervalTree.getIntervals();
		return outputIntervals;
	}

	private void runValidation(FullIntervalTree intervalTree, List<Interval> inputIntervals,
			List<Interval> outputIntervals) {
		List<String> orderErrors = TestValidationUtil.validateIntervalOrder(outputIntervals);
		List<String> accessErrors = TestValidationUtil.validateAccess(intervalTree, inputIntervals);

		System.out.println("------- VALIDATION -------");
		System.out.println();

		System.out.println("Number of Order Errors : " + orderErrors.size());
		orderErrors.forEach(System.out::println);

		System.out.println("Number of Access Errors : " + accessErrors.size());
		accessErrors.forEach(System.out::println);

		System.out.println();
	}

	private void runReport(List<Interval> inputIntervals, List<Interval> outputIntervals) {
		System.out.println("------- REPORT --------");
		System.out.println();

		showIntervals("Test Intervals", inputIntervals);
		showIntervals("Interval Tree", outputIntervals);

		Interval maxInterval = outputIntervals.stream().max(Comparator.comparing(Interval::getSpread)).get();
		Interval minInterval = outputIntervals.stream().min(Comparator.comparing(Interval::getSpread)).get();

		System.out.println("Interval with Max Spread : " + maxInterval + ", distance : " + maxInterval.getSpread());
		System.out.println("Interval with Min Spread : " + minInterval + ", distance : " + minInterval.getSpread());
		System.out.println();
		System.out.println();
	}

	private void showIntervals(String title, List<Interval> intervals) {
		System.out.println(title + " (size=" + intervals.size() + ") : " + intervals);
	}

	private void createOutput(String filename, List<Interval> intervals) {
		if (StringUtils.isNotBlank(filename)) {
			IntervalIO.writeIntervalsToFile(filename, intervals);
		}
	}
}
