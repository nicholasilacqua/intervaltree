package ilacqua.intervaltreeCONDENSED.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;

import ilacqua.intervaltreeCONDENSED.data.Interval;

public class IntervalIO {
	public static void writeIntervalsToFile(String filename, List<Interval> intervals) {
		Path path = Paths.get(filename);

		List<String[]> intervalArrays = intervals.stream()
				.map(interval -> new String[] { interval.getStart().toString(), interval.getEnd().toString() })
				.collect(Collectors.toList());

		try (java.io.BufferedWriter writer = java.nio.file.Files.newBufferedWriter(path)) {
			ICSVWriter csvWriter = new CSVWriterBuilder(writer).build();
			csvWriter.writeAll(intervalArrays);
		} catch (IOException e) {
			System.out.println("FAILURE WRITING FILE");
			e.printStackTrace();
		}
	}

	public static List<Interval> readIntervalsFromFile(String filename) {
		Path path = Paths.get(filename);

		try (BufferedReader reader = java.nio.file.Files.newBufferedReader(path)) {
			CSVReader csvReader = new CSVReaderBuilder(reader).build();
			List<String[]> intervalArrays = csvReader.readAll();

			List<Interval> intervals = intervalArrays.stream().map(IntervalIO::getIntervalFromArray)
					.filter(Objects::nonNull).collect(Collectors.toList());
			return intervals;
		} catch (IOException e) {
			System.out.println("FAILURE READING FILE");
			e.printStackTrace();
			return Collections.emptyList();
		}
	}

	private static Interval getIntervalFromArray(String[] intervalArray) {
		if (intervalArray != null && intervalArray.length == 2) {
			return Interval.of(Integer.valueOf(intervalArray[0]), Integer.valueOf(intervalArray[1]));
		} else {
			return null;
		}
	}
}
