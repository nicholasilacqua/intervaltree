package ilacqua.intervaltreeCONDENSED.util;

import java.util.List;
import java.util.Random;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import ilacqua.intervaltreeCONDENSED.data.Interval;

public class TestDataUtil {

	public static final int DEFAULT_RANDOM_NUM_INTERVALS = 100;
	public static final int DEFAULT_RANDOM_MAX_INTERVAL_SPREAD = 50;
	public static final int DEFAULT_RANDOM_MIN_INTEGER = -5000;
	public static final int DEFAULT_RANDOM_MAX_INTEGER = 5000;

	public static List<Interval> getRandomIntervals() {
		List<Interval> testIntervals = TestDataUtil.getRandomIntervals(DEFAULT_RANDOM_NUM_INTERVALS,
				DEFAULT_RANDOM_MAX_INTERVAL_SPREAD, DEFAULT_RANDOM_MIN_INTEGER, DEFAULT_RANDOM_MAX_INTEGER);
		return testIntervals;
	}

	public static List<Interval> getRandomIntervals(int numIntervals, int maxIntervalSpread, int minInteger,
			int maxInteger) {
		IntFunction<Supplier<Integer>> getIntervalSupplier = base -> () -> base
				+ new Random().nextInt(maxIntervalSpread);
		return new Random().ints(numIntervals, minInteger, maxInteger - maxIntervalSpread).mapToObj(getIntervalSupplier)
				.map(Interval::fromSupplier).collect(Collectors.toList());
	}
}
