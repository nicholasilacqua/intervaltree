package ilacqua.intervaltreeCONDENSED.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ilacqua.intervaltreeCONDENSED.data.Interval;
import ilacqua.intervaltreeCONDENSED.tree.FullIntervalTree;

public class TestValidationUtil {

	public static List<String> validateIntervalOrder(List<Interval> intervals) {
		List<String> errors = new ArrayList<>();

		Integer previousEnd = Integer.MIN_VALUE;
		for (Interval interval : intervals) {
			Integer intervalStart = interval.getStart();
			Integer intervalEnd = interval.getEnd();

			if (intervalStart == null || intervalEnd == null || intervalStart > intervalEnd) {
				errors.add("*** FAILURE : Bad Interval : start=" + intervalStart + ", end=" + intervalEnd);
			}

			if (intervalStart != null && intervalStart <= previousEnd) {
				errors.add("*** FAILURE : Boundary Violation : start=" + intervalStart + ", end=" + intervalEnd
						+ ", boundary=" + previousEnd);
			}

			if (intervalEnd != null) {
				previousEnd = intervalEnd;
			}
		}

		return errors;
	}

	public static List<String> validateAccess(FullIntervalTree intervalTree, List<Interval> testIntervals) {
		List<String> errors = new ArrayList<>();

		Interval realIntervalRange = getRangeAsIntervalFromOrdered(intervalTree.getIntervals());
		Interval testIntervalRange = getRangeAsIntervalFromUnordered(testIntervals);
		if (!realIntervalRange.equals(testIntervalRange)) {
			errors.add("*** FAILURE : Range Violation : realIntervalRange=" + realIntervalRange + ", testIntervalRange="
					+ testIntervalRange);
		}

		for (Interval testInterval : testIntervals) {
			Interval realIntervalStart = intervalTree.findInterval(testInterval.getStart());
			Interval realIntervalEnd = intervalTree.findInterval(testInterval.getEnd());
			if (realIntervalStart == null || realIntervalEnd == null || !realIntervalStart.equals(realIntervalEnd)) {
				errors.add("*** FAILURE : Access Violation : Test to Real : testInterval=" + testInterval
						+ ", realIntervalStart=" + realIntervalStart + ", realIntervalEnd=" + realIntervalEnd);
			}
		}

		for (Interval realInterval : intervalTree.getIntervals()) {
			Integer realIntervalStart = realInterval.getStart();
			Integer realIntervalEnd = realInterval.getEnd();

			if (!isInIntervalList(testIntervals, realIntervalStart)) {
				errors.add("*** FAILURE : Access Violation : Real to Test 1 : realInterval=" + realInterval);
			}
			if (!isInIntervalList(testIntervals, realIntervalEnd)) {
				errors.add("*** FAILURE : Access Violation : Real to Test 2 : realInterval=" + realInterval);
			}

			if (isInIntervalList(testIntervals, realIntervalStart - 1)) {
				errors.add("*** FAILURE : Access Violation : Real to Test 3 : realInterval=" + realInterval);
			}

			if (isInIntervalList(testIntervals, realIntervalEnd + 1)) {
				errors.add("*** FAILURE : Access Violation : Real to Test 4 : realInterval=" + realInterval);
			}
		}

		return errors;
	}

	private static boolean isInIntervalList(List<Interval> intervals, Integer target) {
		return intervals.stream().anyMatch(interval -> interval.withinRange(target));
	}

	private static Interval getRangeAsIntervalFromUnordered(List<Interval> intervals) {
		Integer minStart = intervals.stream().map(Interval::getStart).min(Comparator.naturalOrder()).get();
		Integer maxEnd = intervals.stream().map(Interval::getEnd).max(Comparator.naturalOrder()).get();
		return Interval.of(minStart, maxEnd);
	}

	private static Interval getRangeAsIntervalFromOrdered(List<Interval> intervals) {
		Integer minStart = intervals.get(0).getStart();
		Integer maxEnd = intervals.get(intervals.size() - 1).getEnd();
		return Interval.of(minStart, maxEnd);
	}

}