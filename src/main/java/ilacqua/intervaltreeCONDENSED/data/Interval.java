package ilacqua.intervaltreeCONDENSED.data;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.function.Supplier;

@SuppressWarnings("serial")
public class Interval extends SimpleEntry<Integer, Integer> {

	protected Interval(Integer left, Integer right) {
		super(left, right);
	}

	public Integer getStart() {
		return getKey();
	}

	public Integer getEnd() {
		return getValue();
	}

	public static Interval of(Integer left, Integer right) {
		return new Interval(left, right);
	}

	public Interval clone() {
		return clone(0);
	}

	public Interval clone(int offset) {
		return new Interval(getStart() + offset, getEnd() + offset);
	}

	public boolean withinRange(Integer target) {
		return getStart() <= target && target <= getEnd();
	}

	public Integer getSpread() {
		return getEnd() - getStart();
	}

	public static Interval from(Entry<Integer, Integer> entry) {
		return of(entry.getKey(), entry.getValue());
	}

	public static Interval fromUnordered(Integer param1, Integer param2) {
		return of(Math.min(param1, param2), Math.max(param1, param2));
	}

	public static Interval fromSupplier(Supplier<Integer> supplier) {
		return fromUnordered(supplier.get(), supplier.get());
	}

	public String toString() {
		return "(" + getStart() + " -> " + getEnd() + ")";
	}
}
