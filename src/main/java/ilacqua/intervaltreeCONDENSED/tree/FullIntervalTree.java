package ilacqua.intervaltreeCONDENSED.tree;

import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import ilacqua.intervaltreeCONDENSED.data.Interval;

public class FullIntervalTree {

	TreeMap<Integer, Integer> intervalMap;

	public FullIntervalTree() {
		intervalMap = new TreeMap<>();
	}

	public void insert(Interval interval) {
		Integer start = interval.getStart();
		Integer end = interval.getEnd();

		// Invalid Start/End
		if (start > end) {
			return;
		}

		// Item's interval is already blocked off
		Entry<Integer, Integer> startEntry = intervalMap.floorEntry(start);
		if (startEntry != null && startEntry.getValue() >= end) {
			return;
		}

		Integer startBoundary = start - 1;
		Integer endBoundary = end + 1;

		// Checking if Need to clear area
		Entry<Integer, Integer> endBoundaryEntry = intervalMap.floorEntry(endBoundary);
		if (endBoundaryEntry != null && endBoundaryEntry.getValue() >= startBoundary) {

			if (startEntry != null && startEntry.getKey() <= startBoundary && startEntry.getValue() >= startBoundary) {
				// if lowerStartEntry is below lowerBoundry use it for start
				start = startEntry.getKey();
			} else {
				// check if there's a lower boundary that qualifies, is so use it
				Entry<Integer, Integer> startBoundaryEntry = intervalMap.floorEntry(startBoundary);
				if (startBoundaryEntry != null && startBoundaryEntry.getValue() >= startBoundary) {
					start = startBoundaryEntry.getKey();
				}
			}

			// Check if end should be changed
			if (endBoundaryEntry.getValue() >= endBoundary) {
				end = endBoundaryEntry.getValue();
			}

			NavigableMap<Integer, Integer> subMap = intervalMap.subMap(start + 1, true, end, true);
			subMap.clear();
		}

		intervalMap.put(start, end);
	}

	public Interval findInterval(Integer item) {
		Entry<Integer, Integer> entry = intervalMap.floorEntry(item);
		return entry != null && item <= entry.getValue() ? Interval.from(entry) : null;
	}

	public List<Interval> getIntervals() {
		return intervalMap.entrySet().stream().map(Interval::from).collect(Collectors.toList());
	}

	public void clear() {
		intervalMap.clear();
	}

	public boolean isInInterval(Integer item) {
		return findInterval(item) != null;
	}

	public int getSize() {
		return getIntervals().size();
	}

	@Override
	public String toString() {
		return getIntervals().toString();
	}
}
