package ilacqua.intervaltree.driver;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

import ilacqua.intervaltree.data.IInterval;
import ilacqua.intervaltree.tree.IIntervalTree;
import ilacqua.intervaltree.tree.IIntervalTree.TreeType;
import ilacqua.intervaltree.util.TestValidationUtil;
import ilacqua.intervaltree.util.TestDataUtil;
import ilacqua.intervaltree.util.TestDataUtil.TestDataType;

public class TestDriver {

	private IIntervalTree intervalTree;
	private List<IInterval> testIntervals = new ArrayList<>();
	private Consumer<IInterval> insertAction;
	private int insertGroupSize = 2;

	private TestDataType testDataType = TestDataType.Intersection;
	private TreeType treeType = TreeType.VALUELESS_MAP_INTERVAL;
	private boolean isIntersectionTest;

	private static boolean runAllVariations = true;
	private static boolean debug = false;

	public TestDriver() {
		// Use Default Settings
	}

	public TestDriver(TreeType treeType, TestDataType testDataTypeOverride) {
		this.testDataType = testDataTypeOverride;
		this.treeType = treeType;
	}

	public static void main(String... args) {
		if (runAllVariations) {
			for (TreeType treeTypeOverride : TreeType.values()) {
				for (TestDataType testDataTypeOverride : TestDataType.values()) {
					new TestDriver(treeTypeOverride, testDataTypeOverride).run();
				}
			}
		} else {
			new TestDriver().run();
		}
	}

	private void run() {
		runHeader();
		runTree();
		if (!isIntersectionTest) {
			runValidation();
			runReport();
		}
	}

	private void runHeader() {
		System.out.println("************************************************");
		System.out.println();
		System.out.println("------- INTERVAL TREE -------");
		System.out.println("Tree Type = " + treeType);
		System.out.println("Test Data Type = " + testDataType);
		System.out.println();
	}

	private void runTree() {
		isIntersectionTest = testDataType == TestDataType.Intersection;
		intervalTree = IIntervalTree.getIntervalTree(treeType);
		insertAction = debug || isIntersectionTest ? this::localInsert : intervalTree::insert;

		List<IInterval> intervals = TestDataUtil.getTestData(testDataType);
		intervals.forEach(insertAction);
		if (!isIntersectionTest) {
			this.testIntervals = intervals;
		}
	}

	private void localInsert(IInterval interval) {
		intervalTree.insert(interval);

		if (isIntersectionTest) {
			testIntervals.add(interval);
		}

		if (debug) {
			showIntervals("Test Intervals", testIntervals);
			showIntervals("Interval Tree", intervalTree.getIntervals());
		}

		if (isIntersectionTest && testIntervals.size() == insertGroupSize) {
			TestValidationUtil.validateSingleInterval(intervalTree.getIntervals(), testIntervals);
			runValidation();
			intervalTree.clear();
			testIntervals.clear();
		}
	}

	private void runValidation() {
		List<String> orderErrors = TestValidationUtil.validateIntervalOrder(intervalTree.getIntervals());
		List<String> accessErrors = TestValidationUtil.validateAccess(intervalTree, testIntervals);

		if (isIntersectionTest) {
			if (!orderErrors.isEmpty() || !accessErrors.isEmpty()) {
				System.out.println("Errors : Order " + orderErrors.size() + ", Access : " + accessErrors.size());
				orderErrors.forEach(System.out::println);
				accessErrors.forEach(System.out::println);
			}
		} else {
			System.out.println("------- VALIDATION -------");
			System.out.println();

			System.out.println("Number of Order Errors : " + orderErrors.size());
			orderErrors.forEach(System.out::println);

			System.out.println("Number of Access Errors : " + accessErrors.size());
			accessErrors.forEach(System.out::println);

			System.out.println();
		}
	}

	private void runReport() {
		System.out.println("------- REPORT --------");
		System.out.println();

		showIntervals("Test Intervals", testIntervals);
		showIntervals("Interval Tree", intervalTree.getIntervals());

		IInterval maxInterval = intervalTree.getIntervals().stream().max(Comparator.comparing(IInterval::getSpread))
				.get();
		IInterval minInterval = intervalTree.getIntervals().stream().min(Comparator.comparing(IInterval::getSpread))
				.get();

		System.out.println("Interval with Max Spread : " + maxInterval + ", distance : " + maxInterval.getSpread());
		System.out.println("Interval with Min Spread : " + minInterval + ", distance : " + minInterval.getSpread());
		System.out.println();
		System.out.println();
	}

	private void showIntervals(String title, List<IInterval> intervals) {
		System.out.println(title + " (size=" + intervals.size() + ") : " + intervals);
	}

}
