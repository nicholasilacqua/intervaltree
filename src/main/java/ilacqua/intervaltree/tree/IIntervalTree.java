package ilacqua.intervaltree.tree;

import java.util.List;

import ilacqua.intervaltree.data.IInterval;

public interface IIntervalTree {

	void insert(IInterval interval);

	IInterval findInterval(Integer item);

	void clear();

	List<IInterval> getIntervals();

	default boolean isInInterval(Integer item) {
		return findInterval(item) != null;
	}

	default int getSize() {
		return getIntervals().size();
	}

	default String toPrettyString() {
		return getIntervals().toString();
	}

	public static enum TreeType {
		SIMPLE, FULL, VALUELESS_MAP_INTERVAL
	}

	static IIntervalTree getIntervalTree(TreeType treeType) {
		switch (treeType) {
		case SIMPLE:
			return new SimpleIntervalTree();
		case FULL:
			return new FullIntervalTree();
		case VALUELESS_MAP_INTERVAL:
			return new ValuelessMapIntervalTree();
		default:
			return null;
		}
	}
}
