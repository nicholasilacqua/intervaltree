package ilacqua.intervaltree.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

import ilacqua.intervaltree.data.IInterval;
import ilacqua.intervaltree.data.SortableInterval;

public class ValuelessMapIntervalTree implements IIntervalTree {

	TreeMap<SortableInterval, Void> intervalMap;

	public ValuelessMapIntervalTree() {
		intervalMap = new TreeMap<SortableInterval, Void>();
	}

	@Override
	public void insert(IInterval interval) {
		// Invalid Start/End
		if (interval.getStart() > interval.getEnd()) {
			return;
		}

		SortableInterval sortableInterval = IInterval.sortable().from(interval);

		SortableInterval start = sortableInterval.ofStart();
		SortableInterval end = sortableInterval.ofEnd();

		// Item's interval is already blocked off
		SortableInterval startEntry = getFloor(sortableInterval);
		if (startEntry != null && startEntry.compareEnd(end) >= 0) {
			return;
		}

		SortableInterval startBoundary = start.ofShift(-1);
		SortableInterval endBoundary = end.ofShift(1);

		// Checking if Need to clear area
		SortableInterval endBoundaryEntry = getFloor(endBoundary);
		if (endBoundaryEntry != null && endBoundaryEntry.compareEnd(startBoundary) >= 0) {

			if (startEntry != null && startEntry.compareStart(startBoundary) <= 0
					&& startEntry.compareEnd(startBoundary) >= 0) {
				// if lowerStartEntry is below lowerBoundry use it for start
				start = startEntry.ofStart();
			} else {
				// check if there's a lower boundary that qualifies, is so use it
				SortableInterval startBoundaryEntry = getFloor(startBoundary);
				if (startBoundaryEntry != null && startBoundaryEntry.compareEnd(startBoundary) >= 0) {
					start = startBoundaryEntry.ofStart();
				}
			}

			// Check if end should be changed
			if (endBoundaryEntry.compareEnd(endBoundary) >= 0) {
				end = endBoundaryEntry.ofEnd();
			}

			NavigableMap<SortableInterval, Void> subMap = intervalMap.subMap(start, true, end, true);
			subMap.clear();
		}

		intervalMap.put(IInterval.sortable().joinWithFunc(IInterval::getStart, IInterval::getEnd, start, end), null);
	}

	private SortableInterval getFloor(SortableInterval targetInterval) {
		Entry<SortableInterval, Void> intervalRaw = intervalMap.floorEntry(targetInterval);
		SortableInterval returnInterval = intervalRaw != null ? intervalRaw.getKey() : null;
		return returnInterval;
	}

	@Override
	public IInterval findInterval(Integer target) {
		SortableInterval sortableTarget = IInterval.sortable().of(target, target);
		SortableInterval entry = getFloor(sortableTarget);
		return entry != null && entry.withinRange(target) ? entry : null;
	}

	@Override
	public List<IInterval> getIntervals() {
		return new ArrayList<>(intervalMap.keySet());
	}

	@Override
	public void clear() {
		intervalMap.clear();
	}

	@Override
	public String toString() {
		return toPrettyString();
	}
}
