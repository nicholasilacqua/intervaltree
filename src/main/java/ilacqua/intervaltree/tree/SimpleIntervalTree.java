package ilacqua.intervaltree.tree;

import java.util.List;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import ilacqua.intervaltree.data.IInterval;
import ilacqua.intervaltree.data.Interval;

public class SimpleIntervalTree implements IIntervalTree {

	TreeMap<Integer, Integer> intervalMap;

	public SimpleIntervalTree() {
		intervalMap = new TreeMap<>();
	}

	@Override
	public void insert(IInterval interval) {
		Integer start = interval.getStart();
		Integer end = interval.getEnd();

		if (start > end) {
			return;
		}

		Integer lowerBoundry = start - 1;
		Integer upperBoundry = end + 1;

		Interval lowerInterval = findInterval(lowerBoundry);
		Interval upperInterval = findInterval(upperBoundry);

		if (lowerInterval != null) {
			start = lowerInterval.getKey();
		}
		if (upperInterval != null) {
			end = upperInterval.getValue();
		}

		NavigableMap<Integer, Integer> subMap = intervalMap.subMap(start, true, end, true);
		subMap.clear();

		intervalMap.put(start, end);
	}

	@Override
	public void clear() {
		intervalMap.clear();
	}

	@Override
	public Interval findInterval(Integer item) {
		Entry<Integer, Integer> entry = intervalMap.floorEntry(item);
		return entry != null && item <= entry.getValue() ? IInterval.normal().from(entry) : null;
	}

	@Override
	public List<IInterval> getIntervals() {
		return intervalMap.entrySet().stream().map(IInterval.normal()::from).collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return toPrettyString();
	}
}
