package ilacqua.intervaltree.tree;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * This is the original version I wrote. Josh sent me an email with the
 * specifications. The next morning I woke a little early, around 5:30 or 6 and
 * for whatever reason checked my phone and saw the email. The problem looked
 * interesting. So though I tried to go back to sleep I kept thinking about it,
 * and starting thinking about how would do it, including ending up looking at
 * the java.util.TreeMap source on my phone in bed to check some random detail.
 * I decided on my solution and did research to see what the problem was called,
 * it was an interval tree, and found my solution was called an augmented
 * interval tree. Then I got up and coded it. This is what i sent to Josh that
 * morning before I went to work. Most I wanted to check if the algorithm was
 * generally what other people did, in terms of not going crazy with custom data
 * structures.
 * 
 * 
 * @author nicholasilacqua
 *
 */

public class OriginalntervalTree {

	Map<Integer, Integer> badMap = new LinkedHashMap<Integer, Integer>();
	TreeMap<Integer, Integer> goodMap = new TreeMap<Integer, Integer>();

	public static void main(String... args) {
		new OriginalntervalTree().run();
	}

	private void run() {
		setBadData();
		setGoodData();
	}

	private void setBadData() {
		badMap.put(37, 69);
		badMap.put(190, 235);
		badMap.put(270, 280);
		badMap.put(250, 260);
		badMap.put(200, 290);
		badMap.put(1, 2);

		System.out.println("Initial Data " + badMap);
	}

	private void setGoodData() {
		badMap.forEach(this::insertIntoGoodMap);
		System.out.println("End Data " + goodMap);
	}

	private void insertIntoGoodMap(Integer start, Integer end) {
		if (start >= end) {
			return;
		}

		Entry<Integer, Integer> lowerEntry = goodMap.lowerEntry(start);
		if (lowerEntry != null && lowerEntry.getValue() >= start) {
			start = lowerEntry.getKey();
		}

		NavigableMap<Integer, Integer> subMap = goodMap.subMap(start, true, end, true);
		if (!subMap.isEmpty()) {
			end = Math.max(end, subMap.lastEntry().getValue());
			subMap.clear();
		}
		goodMap.put(start, end);
	}
}
