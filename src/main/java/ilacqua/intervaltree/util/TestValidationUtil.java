package ilacqua.intervaltree.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ilacqua.intervaltree.data.IInterval;
import ilacqua.intervaltree.data.Interval;
import ilacqua.intervaltree.tree.IIntervalTree;

public class TestValidationUtil {

	public static void validateSingleInterval(List<IInterval> realIntervals, List<IInterval> testIntervals) {
		if (realIntervals.size() != 1) {
			System.out.println("*** FAILURE : Only 1 Interval Expected");
			return;
		}

		IInterval expectedInterval = getRangeAsIntervalFromUnordered(testIntervals);
		IInterval realInterval = realIntervals.get(0);
		if (!expectedInterval.equals(realInterval)) {
			System.out.println("*** FAILURE : Real and Expected Intervals don't Match. Real Interval : " + realInterval
					+ ", Expected Interval : " + expectedInterval);
		}
	}

	public static List<String> validateIntervalOrder(List<IInterval> intervals) {
		List<String> errors = new ArrayList<>();

		Integer previousEnd = Integer.MIN_VALUE;
		for (IInterval interval : intervals) {
			Integer intervalStart = interval.getStart();
			Integer intervalEnd = interval.getEnd();

			if (intervalStart == null || intervalEnd == null || intervalStart > intervalEnd) {
				errors.add("*** FAILURE : Bad Interval : start=" + intervalStart + ", end=" + intervalEnd);
			}

			if (intervalStart != null && intervalStart <= previousEnd) {
				errors.add("*** FAILURE : Boundary Violation : start=" + intervalStart + ", end=" + intervalEnd
						+ ", boundary=" + previousEnd);
			}

			if (intervalEnd != null) {
				previousEnd = intervalEnd;
			}
		}

		return errors;
	}

	public static List<String> validateAccess(IIntervalTree intervalTree, List<IInterval> testIntervals) {
		List<String> errors = new ArrayList<>();

		IInterval realIntervalRange = getRangeAsIntervalFromOrdered(intervalTree.getIntervals());
		IInterval testIntervalRange = getRangeAsIntervalFromUnordered(testIntervals);
		if (!realIntervalRange.equals(testIntervalRange)) {
			errors.add("*** FAILURE : Range Violation : realIntervalRange=" + realIntervalRange + ", testIntervalRange="
					+ testIntervalRange);
		}

		for (IInterval testInterval : testIntervals) {
			IInterval realIntervalStart = intervalTree.findInterval(testInterval.getStart());
			IInterval realIntervalEnd = intervalTree.findInterval(testInterval.getEnd());
			if (realIntervalStart == null || realIntervalEnd == null || !realIntervalStart.equals(realIntervalEnd)) {
				errors.add("*** FAILURE : Access Violation : Test to Real : testInterval=" + testInterval
						+ ", realIntervalStart=" + realIntervalStart + ", realIntervalEnd=" + realIntervalEnd);
			}
		}

		for (IInterval realInterval : intervalTree.getIntervals()) {
			Integer realIntervalStart = realInterval.getStart();
			Integer realIntervalEnd = realInterval.getEnd();

			if (!isInIntervalList(testIntervals, realIntervalStart)) {
				errors.add("*** FAILURE : Access Violation : Real to Test 1 : realInterval=" + realInterval);
			}
			if (!isInIntervalList(testIntervals, realIntervalEnd)) {
				errors.add("*** FAILURE : Access Violation : Real to Test 2 : realInterval=" + realInterval);
			}

			if (isInIntervalList(testIntervals, realIntervalStart - 1)) {
				errors.add("*** FAILURE : Access Violation : Real to Test 3 : realInterval=" + realInterval);
			}

			if (isInIntervalList(testIntervals, realIntervalEnd + 1)) {
				errors.add("*** FAILURE : Access Violation : Real to Test 4 : realInterval=" + realInterval);
			}
		}

		return errors;
	}

	private static boolean isInIntervalList(List<IInterval> intervals, Integer target) {
		return intervals.stream().anyMatch(interval -> interval.withinRange(target));
	}

	private static Interval getRangeAsIntervalFromUnordered(List<IInterval> intervals) {
		Integer minStart = intervals.stream().map(IInterval::getStart).min(Comparator.naturalOrder()).get();
		Integer maxEnd = intervals.stream().map(IInterval::getEnd).max(Comparator.naturalOrder()).get();
		return IInterval.normal().of(minStart, maxEnd);
	}

	private static Interval getRangeAsIntervalFromOrdered(List<IInterval> intervals) {
		Integer minStart = intervals.get(0).getStart();
		Integer maxEnd = intervals.get(intervals.size() - 1).getEnd();
		return IInterval.normal().of(minStart, maxEnd);
	}

}