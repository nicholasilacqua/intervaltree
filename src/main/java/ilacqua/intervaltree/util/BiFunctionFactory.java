package ilacqua.intervaltree.util;

import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class BiFunctionFactory<RET, PARAM1, PARAM2> {
	private BiFunction<PARAM1, PARAM2, RET> constructor;

	public BiFunctionFactory(BiFunction<PARAM1, PARAM2, RET> constructor) {
		this.constructor = constructor;
	}

	public RET of(PARAM1 left, PARAM2 right) {
		return constructor.apply(left, right);
	}

	public RET from(Entry<PARAM1, PARAM2> entry) {
		return of(entry.getKey(), entry.getValue());
	}

	public RET fromWithFunc(BiFunction<PARAM1, PARAM2, PARAM1> leftFunc, BiFunction<PARAM1, PARAM2, PARAM2> rightFunc,
			PARAM1 param1, PARAM2 param2) {
		return of(leftFunc.apply(param1, param2), rightFunc.apply(param1, param2));
	}

	public RET joinWithFunc(Function<RET, PARAM1> leftFunc, Function<RET, PARAM2> rightFunc, RET leftRet, RET rightRet) {
		return of(leftFunc.apply(leftRet), rightFunc.apply(rightRet));
	}

	public static class BiIntegerFunctionFactory<RET> extends BiFunctionFactory<RET, Integer, Integer> {

		public BiIntegerFunctionFactory(BiFunction<Integer, Integer, RET> constructor) {
			super(constructor);
		}

		public RET fromUnordered(Integer param1, Integer param2) {
			return fromWithFunc(Math::min, Math::max, param1, param2);
		}

		public RET fromSupplier(Supplier<Integer> supplier) {
			return fromUnordered(supplier.get(), supplier.get());
		}
	}
}
