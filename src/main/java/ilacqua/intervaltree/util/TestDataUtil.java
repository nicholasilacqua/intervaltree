package ilacqua.intervaltree.util;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.IntFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

import ilacqua.intervaltree.data.IInterval;
import ilacqua.intervaltree.data.Interval;

public class TestDataUtil {

	public enum TestDataType {
		Intersection, Series, Random
	}

	private static final int RANDOM_NUM_INTERVALS = 100;
	private static final int MAX_INTERVAL_SPREAD = 50;
	private static final int RANDOM_MIN_INTEGER = -5000;
	private static final int RANDOM_MAX_INTEGER = 5000;

	private final List<IInterval> testIntervals = new ArrayList<>();
	private int intervalGroupCount = 0;

	private TestDataUtil() {
	}

	public static List<IInterval> getTestData(TestDataType testDataType) {
		if (testDataType == null) {
			throw new InvalidParameterException("Test Data Type required");
		}
		switch (testDataType) {
		case Intersection:
			return new TestDataUtil().getIntersectionTestData();
		case Random:
			return getRandom4(RANDOM_NUM_INTERVALS, MAX_INTERVAL_SPREAD, RANDOM_MIN_INTEGER, RANDOM_MAX_INTEGER);
		case Series:
			return new TestDataUtil().getSeriesTestData();
		default:
			return null;
		}
	}

	// Methods to Get Test Intervals

	// Series Intervals

	private List<IInterval> getSeriesTestData() {
		getForwardAndReverseTests(IInterval.normal().of(0, 10), IInterval.normal().of(20, 30),
				IInterval.normal().of(40, 50));
		getForwardAndReverseTests(IInterval.normal().of(20, 30), IInterval.normal().of(0, 10),
				IInterval.normal().of(40, 50));
		getForwardAndReverseTests(IInterval.normal().of(0, 10), IInterval.normal().of(40, 50),
				IInterval.normal().of(20, 30));

		return testIntervals;
	}

	// Intersection Intervals

	private List<IInterval> getIntersectionTestData() {
		// Complete Embedding
		getForwardAndReverseTests(IInterval.normal().of(1, 2), IInterval.normal().of(0, 4));
		getForwardAndReverseTests(IInterval.normal().of(1, 5), IInterval.normal().of(0, 6));
		getForwardAndReverseTests(IInterval.normal().of(2, 5), IInterval.normal().of(0, 6));
		getForwardAndReverseTests(IInterval.normal().of(3, 5), IInterval.normal().of(4, 6));
		getForwardAndReverseTests(IInterval.normal().of(3, 5), IInterval.normal().of(1, 7));

		// Embedding at Beginning
		getForwardAndReverseTests(IInterval.normal().of(0, 1), IInterval.normal().of(0, 2));
		getForwardAndReverseTests(IInterval.normal().of(0, 2), IInterval.normal().of(0, 3));
		getForwardAndReverseTests(IInterval.normal().of(0, 2), IInterval.normal().of(0, 4));

		// Embedding at End
		getForwardAndReverseTests(IInterval.normal().of(0, 2), IInterval.normal().of(1, 2));
		getForwardAndReverseTests(IInterval.normal().of(0, 4), IInterval.normal().of(1, 4));
		getForwardAndReverseTests(IInterval.normal().of(0, 4), IInterval.normal().of(2, 4));
		getForwardAndReverseTests(IInterval.normal().of(0, 4), IInterval.normal().of(3, 4));

		// Adjacent
		getForwardAndReverseTests(IInterval.normal().of(0, 1), IInterval.normal().of(2, 3));
		getForwardAndReverseTests(IInterval.normal().of(0, 2), IInterval.normal().of(3, 5));

		// Overlap
		getForwardAndReverseTests(IInterval.normal().of(0, 2), IInterval.normal().of(1, 3));
		getForwardAndReverseTests(IInterval.normal().of(0, 3), IInterval.normal().of(2, 5));

		// Single Point Intervals
		getForwardAndReverseTests(IInterval.normal().of(0, 2), IInterval.normal().of(1, 1));
		getForwardAndReverseTests(IInterval.normal().of(0, 4), IInterval.normal().of(2, 2));
		getForwardAndReverseTests(IInterval.normal().of(0, 0), IInterval.normal().of(0, 1));
		getForwardAndReverseTests(IInterval.normal().of(0, 0), IInterval.normal().of(0, 2));
		getForwardAndReverseTests(IInterval.normal().of(0, 1), IInterval.normal().of(1, 1));
		getForwardAndReverseTests(IInterval.normal().of(0, 2), IInterval.normal().of(2, 2));

		return testIntervals;
	}

	private void getForwardAndReverseTests(Interval... intervals) {
		addIntervalGroups(intervals);
		ArrayUtils.reverse(intervals);
		addIntervalGroups(intervals);
	}

	private void addIntervalGroups(Interval... intervals) {
		int offset = generateIntervalGroupOffset();
		for (Interval interval : intervals) {
			testIntervals.add(interval.clone(offset));
		}
	}

	private int generateIntervalGroupOffset() {
		return 100 * intervalGroupCount++;
	}

	//
	// Random Intervals
	//

	// Random4 is Method that is the current method in use
	@Deprecated
	private static List<IInterval> getRandom1(int numIntervals, int maxIntervalSpread, int minInteger, int maxInteger) {
		List<IInterval> testIntervals = new ArrayList<>();
		int integerRange = Math.abs(maxInteger - minInteger) - maxIntervalSpread;

		for (int i = 0; i < numIntervals; i++) {
			int base = new Random().nextInt(integerRange) + minInteger;
			int a = base + new Random().nextInt(maxIntervalSpread);
			int b = base + new Random().nextInt(maxIntervalSpread);
			Interval interval = IInterval.normal().fromUnordered(a, b);
			testIntervals.add(interval);
		}

		return testIntervals;
	}

	// Random4 is Method that is the current method in use
	@Deprecated
	private static List<IInterval> getRandom2(int numIntervals, int maxIntervalSpread, int minInteger, int maxInteger) {
		List<IInterval> testIntervals = new ArrayList<>();
		int integerRange = Math.abs(maxInteger - minInteger) - maxIntervalSpread;

		Supplier<Integer> getIntegerBase = () -> new Random().nextInt(integerRange) + minInteger;
		Supplier<Integer> getIntegerSpread = () -> new Random().nextInt(maxIntervalSpread);
		Supplier<Interval> getInterval = () -> {
			int base = getIntegerBase.get();
			return IInterval.normal().fromUnordered(base + getIntegerSpread.get(), base + getIntegerSpread.get());
		};

		for (int i = 0; i < numIntervals; i++) {
			testIntervals.add(getInterval.get());
		}

		return testIntervals;
	}

	// Random4 is Method that is the current method in use
	@Deprecated
	private static List<IInterval> getRandom3(int numIntervals, int maxIntervalSpread, int minInteger, int maxInteger) {
		List<IInterval> testIntervals = new ArrayList<>();
		int integerRange = Math.abs(maxInteger - minInteger) - maxIntervalSpread;

		BiFunction<Integer, Integer, Integer> getRandom = (range, offset) -> offset + new Random().nextInt(range);
		Supplier<Interval> getInterval = () -> {
			Integer base = getRandom.apply(integerRange, minInteger);
			return IInterval.normal().fromUnordered(getRandom.apply(maxIntervalSpread, base),
					getRandom.apply(maxIntervalSpread, base));
		};

		for (int i = 0; i < numIntervals; i++) {
			testIntervals.add(getInterval.get());
		}

		return testIntervals;
	}

	// This is the Method that the code is using
	private static List<IInterval> getRandom4(int numIntervals, int maxIntervalSpread, int minInteger, int maxInteger) {
		IntFunction<Supplier<Integer>> getIntervalSupplier = base -> () -> base
				+ new Random().nextInt(maxIntervalSpread);
		return new Random().ints(numIntervals, minInteger, maxInteger - maxIntervalSpread).mapToObj(getIntervalSupplier)
				.map(IInterval.normal()::fromSupplier).collect(Collectors.toList());
	}

}
