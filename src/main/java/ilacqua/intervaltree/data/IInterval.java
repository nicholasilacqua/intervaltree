package ilacqua.intervaltree.data;

import java.util.Map.Entry;

import ilacqua.intervaltree.util.BiFunctionFactory.BiIntegerFunctionFactory;

public interface IInterval extends Entry<Integer, Integer> {
	public static BiIntegerFunctionFactory<Interval> normal() {
		return new BiIntegerFunctionFactory<Interval>(Interval::new);
	}

	public static BiIntegerFunctionFactory<SortableInterval> sortable() {
		return new BiIntegerFunctionFactory<SortableInterval>(SortableInterval::new);
	}

	Integer getStart();

	Integer getEnd();

	public default boolean withinRange(Integer target) {
		return getStart() <= target && target <= getEnd();
	}

	public default Integer getSpread() {
		return getEnd() - getStart();
	}

	public default String toPrettyString() {
		return "(" + getStart() + " -> " + getEnd() + ")";
	}
}
