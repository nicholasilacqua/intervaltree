package ilacqua.intervaltree.data;

import java.util.AbstractMap.SimpleEntry;
import java.util.Comparator;

@SuppressWarnings("serial")
public class SortableInterval extends SimpleEntry<Integer, Integer> implements IInterval, Comparable<SortableInterval> {

	protected SortableInterval(Integer left, Integer right) {
		super(left, right);
	}

	public Integer getStart() {
		return getKey();
	}

	public Integer getEnd() {
		return getValue();
	}

	public SortableInterval clone() {
		return clone(0);
	}

	public SortableInterval clone(int offset) {
		return new SortableInterval(getStart() + offset, getEnd() + offset);
	}

	public String toString() {
		return toPrettyString();
	}

	// Point Intervals

	protected SortableInterval(Integer point) {
		super(point, point);
	}

	public SortableInterval ofStart() {
		return new SortableInterval(getStart());
	}

	public SortableInterval ofEnd() {
		return new SortableInterval(getEnd());
	}

	public SortableInterval ofShift(int i) {
		return new SortableInterval(getStart() + i);
	}

	// Comparators

	@Override
	public int compareTo(SortableInterval other) {
		return compareStart(other);
	}

	public int compareStart(SortableInterval other) {
		return Integer.compare(getStart(), other.getStart());
	}

	public int compareEnd(SortableInterval other) {
		return Integer.compare(getEnd(), other.getEnd());
	}

	public static Comparator<SortableInterval> getComparator(boolean sortEndFirst, boolean sortStartAndEnd,
			boolean reverse) {
		Comparator<SortableInterval> comparator = sortEndFirst ? SortableInterval::compareEnd
				: SortableInterval::compareStart;

		if (sortStartAndEnd) {
			Comparator<SortableInterval> thenCompare = sortEndFirst ? SortableInterval::compareStart
					: SortableInterval::compareEnd;
			comparator = comparator.thenComparing(thenCompare);
		}

		if (reverse) {
			comparator = comparator.reversed();
		}

		return comparator;
	}
}
