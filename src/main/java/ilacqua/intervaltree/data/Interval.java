package ilacqua.intervaltree.data;

import java.util.AbstractMap.SimpleEntry;

@SuppressWarnings("serial")
public class Interval extends SimpleEntry<Integer, Integer> implements IInterval {

	protected Interval(Integer left, Integer right) {
		super(left, right);
	}

	public Integer getStart() {
		return getKey();
	}

	public Integer getEnd() {
		return getValue();
	}

	public Interval clone() {
		return clone(0);
	}

	public Interval clone(int offset) {
		return new Interval(getStart() + offset, getEnd() + offset);
	}

	public String toString() {
		return toPrettyString();
	}
}
